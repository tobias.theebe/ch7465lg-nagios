<?php
error_reporting(E_ALL ^ E_WARNING);

$function = $argv[1];
$host =     $argv[2];
$pass =     $argv[3];

$ua =       'debug_ch7465lg.php';
$url_auth = 'http://' . $host . '/common_page/login.html';
$url_set =  'http://' . $host . '/xml/setter.xml';
$url_get =  'http://' . $host . '/xml/getter.xml';
$st =       [];
$sid =      [];

doLogin();
dumpData($function);
doLogout();

function doLogin() {
	global $url_auth, $url_set, $ua, $st, $sid, $pass;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'HEAD');
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_auth);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$login1 = curl_exec($ch);
	curl_close($ch);

	preg_match('/sessionToken=(.*?);/', $login1, $st);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: sessionToken=' . $st[1], 'Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "token=" . $st[1] . "&fun=15&Username=NULL&Password=" . $pass);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_set);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$login2 = curl_exec($ch);
	curl_close($ch);

	preg_match('/sessionToken=(.*?);/', $login2, $st);
	preg_match('/SID=(.*)/', $login2, $sid);

	if (!$st[1] || !$sid[1]) {echo "Error: unable to create session.\n"; exit();}
}

function doLogout() {
	global $url_set, $ua, $st, $sid;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'HEAD');
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: sessionToken=' . $st[1], 'Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "token=" . $st[1] . "&fun=16");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_set);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$logout = curl_exec($ch);
	curl_close($ch);
}

function dumpData($fun) {
	global $url_get, $ua, $st, $sid;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: sessionToken=' . $st[1], 'Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, 'token=' . $st[1] . '&fun=' . $fun);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_get);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$data_xml = curl_exec($ch);
	unset($ch); unset($data);

	preg_match('/sessionToken=(.*?);/', $data_xml, $st);
	preg_match('/\n(.*)$/i', $data_xml, $data_xml_b);
	
	if (!$data_xml_b[1]) {echo "Error: unable to fetch data.\n";}
	else {print_r(simplexml_load_string($data_xml_b[1])); echo "\n";}
}
?>
