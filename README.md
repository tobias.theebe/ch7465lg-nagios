# Compal CH7465LG Plugin for Nagios

**DUE TO SWITCHING TO A UBEE UBC1318ZG, THIS PROJECT WILL NO LONGER RECEIVE FREQUENT UPDATES.**

Update 2023-06-29: the debug script is still compatible with the latest firmware (`CH7465LG-NCIP-6.15.32p3-NOSH`), [example](https://community.ziggo.nl/t5/Internet/ConnectBox-Loss-of-Sync-multiple-times-per-day/m-p/1211579/highlight/true#M226539).

## Description

This PHP script serves as a simple monitoring solution for the CH7465LG cable modem/router produced by Compal Information (Kunshan) Co., Ltd. The script can retrieve various information from the modem (currently downstream/upstream channels + telephony) and output the information to a Nagios object.

In addition to displaying status information in table views, it outputs performance data that can be used for further processing (RRD files) in graphing applications such as nagiosgraph and PNP4Nagios. This allows the user to observe the modem's behavior over an extended period of time.

The script has been written for and tested with a CH7465LG-ZG provided by Ziggo, a major ISP in the Netherlands. It MIGHT work with CH7465 modems supplied by other providers (Telenet, UPC, Virgin Media).

## Software Requirements

The script has been tested in the following environment:

- Debian 10.3-10.9
- NGINX 1.14.2
- PHP 7.3.19-7.3.27 with cURL and XML modules
- Nagios Core 4.4.6
- NagiosQL 3.4.1
- nagiosgraph 1.5.2

## Installation

1. In the Nagios CGI configuration (*etc/cgi.cfg*), set *escape_html_tags* to **0**.
2. Download *check_ch7465lg_v2.php* and move it to the plugin directory (*libexec/*).
3. Create a new command with the following command line: `php $USER1$/check_ch7465lg_v2.php $ARG1$ $HOSTADDRESS$ PASS`. Replace `PASS` (password, 8 characters in my case) with the appropriate value.
4. Create a new host. Use *check_ping* as the the check command (or *check_tcp*, in case the device is unreachable by ICMP).
5. Create three new services. Use the new command as the check command. Use **channels_ds**, **channels_us** and **telephony** as *$ARG1$*, respectively.
6. Assign the services to the host.
7. Save and verify the new configuration and restart the Nagios daemon.
8. **Optional** - Append the contents of *common.css* to *share/stylesheets/common.css* within your Nagios directory.
9. **Optional** - Append the contents of *labels.conf* to *etc/labels.conf* within your nagiosgraph directory.

## Notes

- Using a configuration management application like NagiosQL is highly recommended.
- In case of RCS/TCS partial service (one or more channels could not be locked onto during the ranging process), the script will exit with a CRITICAL/WARNING state. The script is configured for a 24×4 channel profile by default. If your modem has a different channel profile assigned to it by the CMTS (24×6 for example), you must adjust the *$ds_count_th* and *$us_count_th* variables accordingly. You will find these on line 112 and 156, respectively.
- In order to generate graphs from the channel data, the graphing application needs to be properly configured. Configuring Nagios for performance data processing and configuring an external application to write/read to/from RRD files is beyond the scope of this readme.
