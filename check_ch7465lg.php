<?php
error_reporting(E_ALL ^ E_WARNING);

define('STATE_OK',       0);
define('STATE_WARNING',  1);
define('STATE_CRITICAL', 2);
define('STATE_UNKNOWN',  3);

$option =   $argv[1];

$ua =       'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36';
$host =     '192.168.100.1';
$url_auth = 'http://' . $host . '/common_page/login.html';
$url_set =  'http://' . $host . '/xml/setter.xml';
$url_get =  'http://' . $host . '/xml/getter.xml';
$token =    [];

switch($option) {
	case 'status':      doAuthenticate(); getStatus();       break;
	case 'channels_ds': doAuthenticate(); getChannels('ds'); break;
	case 'channels_us': doAuthenticate(); getChannels('us'); break;
}

function doAuthenticate() {
	global $url_auth, $ua, $token;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'HEAD');
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_auth);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$session = curl_exec($ch);
	unset($ch);

	$session = preg_match('/sessionToken=(.*?);/', $session, $token);

	if (!$token[1]) {echo 'UNKNOWN - Authentication error'; exit(STATE_UNKNOWN);}
}

function getStatus() {
	global $url_get, $ua, $token;

	$data = 'token=' . $token[1] . '&fun=136';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_get);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$status_xml = curl_exec($ch);
	unset($ch); unset($data);

	$status = simplexml_load_string($status_xml)->OperState;

	if (!$status) {echo 'UNKNOWN - No data available'; exit(STATE_UNKNOWN);}
	elseif ($status != 'OPERATIONAL') {echo 'CRITICAL - ' . $status; exit(STATE_CRITICAL);}
	else {echo 'OK'; exit(STATE_OK);}
}

function getChannels($channels_type) {
	global $url_get, $ua, $token;

	$data = 'token=' . $token[1];

	if ($channels_type == 'ds') {$data .= '&fun=10';}
	else {$data .= '&fun=11';}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_get);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$channels_xml = curl_exec($ch);
	unset($ch); unset($data);

	$channels = simplexml_load_string($channels_xml);

	if ($channels_type == 'ds') {
		$i_ds_warn = 0; $i_ds_crit = 0;
		$ds_channel_status = '';
		$ds_channels_html = '<table class="statusInfo"><tr><th>ID</th><th>Frequency</th><th>Modulation</th><th>Power</th><th>SNR</th><th>Post-RS</th></tr>';
		$ds_channels_perf = '';

		if(!$channels->downstream) {echo 'UNKNOWN - No data available'; exit(STATE_UNKNOWN);}

		foreach ($channels->downstream as $ds_channel) {
			if ($ds_channel->pow < -10 || $ds_channel->pow > 10 || $ds_channel->snr < 33 || $ds_channel->mod != '256qam') {
				if ($ds_channel->pow < -15 || $ds_channel->pow > 15 || $ds_channel->snr < 30) {$i_ds_crit++; $ds_channel_status = 'statusCRITICAL';}
				else {$i_ds_warn++; $ds_channel_status = 'statusWARNING';}
			}
			else {$ds_channel_status = 'statusOK';};

			$ds_channels_html .= '<tr><td class="' . $ds_channel_status . '">' . $ds_channel->chid . '</td><td>' . $ds_channel->freq / 1000000 . ' MHz</td><td>' . str_replace('qam', '-QAM', $ds_channel->mod) . '</td><td>' . $ds_channel->pow . ' dBmV</td><td>' . $ds_channel->RxMER . ' dB</td><td>' . $ds_channel->PostRs . '</td></tr>';
			$ds_channels_perf .= 'pwr-ch' . $ds_channel->chid . '=' . $ds_channel->pow . ' snr-ch' . $ds_channel->chid . '=' . $ds_channel->RxMER . ' ';
		}

		$ds_channels_html .= '</table>';
		echo $ds_channels_html . ' | ' . $ds_channels_perf;

		if ($i_ds_crit > 0) {exit(STATE_CRITICAL);}
		elseif ($i_ds_warn > 0) {exit(STATE_WARNING);}
		else {exit(STATE_OK);}
	}
	else {
		$i_us_warn = 0; $i_us_crit = 0;
		$us_channel_status = '';
		$us_channels_html = '<table class="statusInfo"><tr><th>ID</th><th>Frequency</th><th>Modulation</th><th>Type</th><th>Power</th><th>Symbol Rate</th><th>T3</th></tr>';
		$us_channels_perf = '';

		if(!$channels->upstream) {echo 'UNKNOWN - No data available'; exit(STATE_UNKNOWN);}

		foreach ($channels->upstream as $us_channel) {
			if (($us_channel->power > 49 && $us_channel->power < 52) || $us_channel->mod != '64qam') {$i_us_warn++; $us_channel_status = 'statusWARNING';}
			elseif ($us_channel->power >= 52) {$i_us_crit++; $us_channel_status = 'statusCRITICAL';}
			else {$us_channel_status = 'statusOK';}

			$us_channels_html .= '<tr><td class="' . $us_channel_status . '">' . $us_channel->usid . '</td><td>' . number_format($us_channel->freq / 1000000, 1, '.', '') . ' MHz</td><td>' . str_replace('qam', '-QAM', $us_channel->mod) . '</td><td>' . $us_channel->channeltype . '</td><td>' . $us_channel->power . ' dBmV</td><td>' . number_format($us_channel->srate * 1000, 0, '.', ',') . ' ksps</td><td>' . $us_channel->t3Timeouts . '</td></tr>';
			$us_channels_perf .= 'pwr-ch' . $us_channel->usid . '=' . $us_channel->power . ' t3-ch' . $us_channel->usid . '=' . $us_channel->t3Timeouts . ' ';
		}

		$us_channels_html .= '</table>';
		echo $us_channels_html . ' | ' . $us_channels_perf;

		if ($i_us_crit > 0) {exit(STATE_CRITICAL);}
		elseif ($i_us_warn > 0) {exit(STATE_WARNING);}
		else {exit(STATE_OK);}
	}
}
?>
