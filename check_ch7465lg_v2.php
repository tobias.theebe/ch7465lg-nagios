<?php
error_reporting(E_ALL ^ E_WARNING);

define('STATE_OK',       0);
define('STATE_WARNING',  1);
define('STATE_CRITICAL', 2);
define('STATE_UNKNOWN',  3);

$option =   $argv[1];
$host =     $argv[2];
$pass =     $argv[3];

$ua =       'check_ch7465lg_v2.php';
$url_auth = 'http://' . $host . '/common_page/login.html';
$url_set =  'http://' . $host . '/xml/setter.xml';
$url_get =  'http://' . $host . '/xml/getter.xml';
$st =       [];
$sid =      [];

switch($option) {
	case 'channels_ds': doLogin(); getChannels('ds'); break;
	case 'channels_us': doLogin(); getChannels('us'); break;
	case 'telephony': doLogin(); getTelephony(); break;
	default: echo 'UNKNOWN - Invalid option'; exit(STATE_UNKNOWN); break;
}

function doLogin() {
	global $url_auth, $url_set, $ua, $st, $sid, $pass;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'HEAD');
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_auth);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$login1 = curl_exec($ch);
	curl_close($ch);

	preg_match('/sessionToken=(.*?);/', $login1, $st);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: sessionToken=' . $st[1], 'Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "token=" . $st[1] . "&fun=15&Username=NULL&Password=" . $pass);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_set);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$login2 = curl_exec($ch);
	curl_close($ch);

	preg_match('/sessionToken=(.*?);/', $login2, $st);
	preg_match('/SID=(.*)/', $login2, $sid);

	if (!$st[1] || !$sid[1]) {echo 'UNKNOWN - Authentication error'; exit(STATE_UNKNOWN);}
}

function doLogout() {
	global $url_set, $ua, $st, $sid;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'HEAD');
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: sessionToken=' . $st[1], 'Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "token=" . $st[1] . "&fun=16");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_set);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$logout = curl_exec($ch);
	curl_close($ch);
}

function getChannels($channels_type) {
	global $url_get, $ua, $st, $sid;

	$data = 'token=' . $st[1];

	switch($channels_type) {
		case 'ds': $data .= '&fun=10'; break;
		case 'us': $data .= '&fun=11'; break;
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: sessionToken=' . $st[1], 'Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_get);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$channels_xml = curl_exec($ch);
	unset($ch); unset($data);

	preg_match('/sessionToken=(.*?);/', $channels_xml, $st);
	preg_match('/\n(.*)$/i', $channels_xml, $channels_xml_b);
	$channels = simplexml_load_string($channels_xml_b[1]);

	if ($channels_type == 'ds') {
		$i_ds_warn = 0; $i_ds_crit = 0; $ds_count_th = 24;
		$ds_channels_status = ''; $ds_channel_status = '';
		$ds_channels_avg_pwr = 0; $ds_channels_avg_mer = 0; $ds_channels_avg_postrs = 0;
		$ds_channels_html = '<table class="statusInfo"><tr><th>CID</th><th><i>f<sub>c</sub></i></th><th>MT</th><th>LS</th><th><i>P</i></th><th>MER</th><th>Pre-FEC UEC</th><th>Post-FEC CC</th></tr>';
		$ds_channels_perf = '';

		if(!$channels->downstream) {echo 'DS UNKNOWN - No data available'; exit(STATE_UNKNOWN);}

		foreach ($channels->downstream as $ds_channel) {
			if ($ds_channel->pow < -15 || $ds_channel->pow > 15 || $ds_channel->snr < 31 || $ds_channel->IsQamLocked != '1' || $ds_channel->IsFECLocked != '1' || $ds_channel->IsMpegLocked != '1') {$i_ds_crit++; $ds_channel_status = 'statusCRITICAL';}
			elseif ($ds_channel->pow < -8 || $ds_channel->pow > 10 || $ds_channel->snr < 33 || $ds_channel->mod != '256qam') {$i_ds_warn++; $ds_channel_status = 'statusWARNING';}
			else {$ds_channel_status = 'statusOK';}

			$ds_channels_avg_pwr += $ds_channel->pow;
			$ds_channels_avg_mer += $ds_channel->RxMER;
			$ds_channels_avg_postrs += $ds_channel->PostRs;
			$ds_channels_html .= '<tr><td class="' . $ds_channel_status . '">' . $ds_channel->chid . '</td><td>' . $ds_channel->freq / 1000000 . '</td><td>' . str_replace('qam', '-QAM', $ds_channel->mod) . '</td><td>' . ($ds_channel->IsQamLocked == '1' ? 'Q ' : '- ') . ($ds_channel->IsFECLocked == '1' ? 'F ' : '- ') . ($ds_channel->IsMpegLocked == '1' ? 'M' : '-') . '</td><td>' . $ds_channel->pow . '</td><td>' . $ds_channel->RxMER . '</td><td>' . number_format((float)$ds_channel->PreRs, 0, '', ',') . '</td><td>' . number_format((int)$ds_channel->PostRs, 0, '', ',') . '</td></tr>';
			$ds_channels_perf .= 'pwr-ch' . $ds_channel->chid . '=' . $ds_channel->pow . ' snr-ch' . $ds_channel->chid . '=' . $ds_channel->RxMER . ' postrs-ch' . $ds_channel->chid . '=' . $ds_channel->PostRs . ' ';
		}

		if ($i_ds_crit > 0 || $channels->ds_num < ($ds_count_th / 2)) {$ds_channels_status = 'CRITICAL';}
		elseif ($i_ds_warn > 0 || $channels->ds_num < $ds_count_th) {$ds_channels_status = 'WARNING';}
		else {$ds_channels_status = 'OK';}

		$ds_channels_avg_pwr = number_format($ds_channels_avg_pwr / $channels->ds_num, 0, '', ',');
		$ds_channels_avg_mer = number_format($ds_channels_avg_mer / $channels->ds_num, 3, '.', ',');
		$ds_channels_avg_postrs = number_format($ds_channels_avg_postrs / $channels->ds_num, 0, '', ',');
		$ds_channels_html .= '<tr><td style="text-align: center"><i><span style="text-decoration: overline">x</span></i></td><td colspan="3"></td><td><i>' . $ds_channels_avg_pwr . '</i></td><td><i>' . $ds_channels_avg_mer . '</i></td><td></td><td><i>' . $ds_channels_avg_postrs . '</i></td><td></td></tr>';
		$ds_channels_html .= '<tr><td></td><td>MHz</td><td colspan="2"></td><td>dBmV</td><td>dB</td><td colspan="2"></td></tr>';
		$ds_channels_html .= '</table>';
		$ds_channels_html .= 'DS ' . $ds_channels_status . ' - DBG (RCS) = ' . $channels->ds_num . '/' . $ds_count_th . ', C/W = ' . $i_ds_crit . '/' . $i_ds_warn;
		$ds_channels_perf .= 'pwr-avg=' . $ds_channels_avg_pwr . ' snr-avg=' . $ds_channels_avg_mer . ' postrs-avg=' . str_replace(',', '', $ds_channels_avg_postrs) . ' ';
		$ds_channels_perf .= 'pwr-min=U;-8;-15 pwr-max=U;10;15 snr-min=U;33;31';

		echo $ds_channels_html . ' | ' . $ds_channels_perf;

		doLogout();

		switch ($ds_channels_status) {
			case 'CRITICAL': exit(STATE_CRITICAL); break;
			case 'WARNING': exit(STATE_WARNING); break;
			case 'OK': exit(STATE_OK); break;
		}
	}
	elseif ($channels_type == 'us') {
		$i_us_warn = 0; $i_us_crit = 0; $us_count_th = 4;
		$us_channels_status = ''; $us_channel_status = '';
		$us_channels_avg_pwr = 0;
		$us_channels_sum_t1 = 0; $us_channels_sum_t2 = 0; $us_channels_sum_t3 = 0; $us_channels_sum_t4 = 0;
		$us_channels_html = '<table class="statusInfo"><tr><th>CID</th><th><i>f<sub>c</sub></i></th><th>MT</th><th>CT</th><th><i>P</i><th>SR</th><th>T1</th><th>T2</th><th>T3</th><th>T4</th></tr>';
		$us_channels_perf = '';

		if(!$channels->upstream) {echo 'US UNKNOWN - No data available'; exit(STATE_UNKNOWN);}

		foreach ($channels->upstream as $us_channel) {
			if ($us_channel->power > 51 || $us_channel->t1Timeouts > 0 || $us_channel->t2Timeouts > 0) {$i_us_crit++; $us_channel_status = 'statusCRITICAL';}
			elseif ($us_channel->power > 49 || $us_channel->mod != '64qam' || $us_channel->t4Timeouts > 0) {$i_us_warn++; $us_channel_status = 'statusWARNING';}
			else {$us_channel_status = 'statusOK';}

			$us_channels_avg_pwr += $us_channel->power;
			$us_channels_sum_t1 += $us_channel->t1Timeouts;
			$us_channels_sum_t2 += $us_channel->t2Timeouts;
			$us_channels_sum_t3 += $us_channel->t3Timeouts;
			$us_channels_sum_t4 += $us_channel->t4Timeouts;
			$us_channels_html .= '<tr><td class="' . $us_channel_status . '">' . $us_channel->usid . '</td><td>' . number_format($us_channel->freq / 1000000, 6, '.', ',') . '</td><td>' . str_replace('qam', '-QAM', $us_channel->mod) . '</td><td>' . $us_channel->channeltype . '</td><td>' . $us_channel->power . '</td><td>' . number_format($us_channel->srate * 1000, 0, '', ',') . '</td><td>' . number_format((int)$us_channel->t1Timeouts, 0, '', ',') . '</td><td>' . number_format((int)$us_channel->t2Timeouts, 0, '', ',') . '</td><td>' . number_format((int)$us_channel->t3Timeouts, 0, '', ',') . '</td><td>' . number_format((int)$us_channel->t4Timeouts, 0, '', ',') . '</td></tr>';
			$us_channels_perf .= 'pwr-ch' . $us_channel->usid . '=' . $us_channel->power  . ' t1-ch' . $us_channel->usid . '=' . $us_channel->t1Timeouts . ' t2-ch' . $us_channel->usid . '=' . $us_channel->t2Timeouts . ' t3-ch' . $us_channel->usid . '=' . $us_channel->t3Timeouts . ' t4-ch' . $us_channel->usid . '=' . $us_channel->t4Timeouts . ' ';
		}

		if ($i_us_crit > 0 || $channels->us_num < ($us_count_th / 2)) {$us_channels_status = 'CRITICAL';}
		elseif ($i_us_warn > 0 || $channels->us_num < $us_count_th) {$us_channels_status = 'WARNING';}
		else {$us_channels_status = 'OK';}

		$us_channels_avg_pwr = number_format($us_channels_avg_pwr / $channels->us_num, 0, '', ',');
		$us_channels_html .= '<tr><td style="text-align: center"><i><span style="text-decoration: overline">x</span></i></td><td colspan="3"></td><td><i>' . $us_channels_avg_pwr . '</i></td><td colspan="5"></td></tr>';
		$us_channels_html .= '<tr><td style="text-align: center">∑</td><td colspan="5"></td><td><i>' . $us_channels_sum_t1 . '</i></td><td><i>' . $us_channels_sum_t2 . '</i></td><td><i>' . $us_channels_sum_t3 . '</i></td><td><i>' . $us_channels_sum_t4 . '</i></td></tr>';
		$us_channels_html .= '<tr><td></td><td>MHz</td><td colspan="2"></td><td>dBmV</td><td>ks/s</td><td colspan="4"></td></tr>';
		$us_channels_html .= '</table>';
		$us_channels_html .= 'US ' . $us_channels_status . ' - UBG (TCS) = ' . $channels->us_num . '/' . $us_count_th . ', C/W = ' . $i_us_crit . '/' . $i_us_warn;
		$us_channels_perf .= 'pwr-avg=' . $us_channels_avg_pwr . ' t1=' . $us_channels_sum_t1 . ' t2=' . $us_channels_sum_t2 . ' t3=' . $us_channels_sum_t3 . ' t4=' . $us_channels_sum_t4 . ' ';
		$us_channels_perf .= 'pwr-max=U;49;51';

		echo $us_channels_html . ' | ' . $us_channels_perf;

		doLogout();

		switch ($us_channels_status) {
			case 'CRITICAL': exit(STATE_CRITICAL); break;
			case 'WARNING': exit(STATE_WARNING); break;
			case 'OK': exit(STATE_OK); break;
		}
	}
}

function getTelephony() {
	global $url_get, $ua, $st, $sid;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: sessionToken=' . $st[1], 'Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "token=" . $st[1] . "&fun=500");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url_get);
	curl_setopt($ch, CURLOPT_USERAGENT, $ua);
	$telephony_xml = curl_exec($ch);
	curl_close($ch);

	preg_match('/sessionToken=(.*?);/', $telephony_xml, $st);
	preg_match('/\n(.*)$/i', $telephony_xml, $telephony_xml_b);
	$telephony = simplexml_load_string($telephony_xml_b[1]);

	$telephony_status = ''; $telephony_line_status = '';
	$telephony_html = '<table class="statusInfo">';

	if(!$telephony->Line) {echo 'TEL UNKNOWN - No line data available'; exit(STATE_UNKNOWN);}

	foreach ($telephony->Line as $telephony_line) {
		if ($telephony_line->ProvSt != 'operational' || $telephony_line->mtastatus != 'ready') {$telephony_line_status = 'statusCRITICAL';}
		else {$telephony_line_status = 'statusOK';}

		$telephony_html .= '<tr><td class="' . $telephony_line_status . '">FXS ' . $telephony_line->LineNum . '</td><td>' . ($telephony_line->HookSt == 'on_hook' ? 'Idle' : 'Active') . '</td></tr>';
	}

	if ($telephony->Telephone_Registration_Complete != 'Pass' && $telephony->Telephone_Registration_Complete != 'Pass With Warnings') {$telephony_status = 'CRITICAL';}
	else {$telephony_status = 'OK';}

	$telephony_html .= '</tr><tr>';

	if ($telephony->DHCP == 'Completed') {$telephony_html .= '<td class="statusOK">';} else {$telephony_html .= '<td class="statusCRITICAL">';}

	$telephony_html .= 'DHCP</td><td>' . $telephony->DHCP . '</td></tr><tr><td style="text-align: center">SEC</td><td>' . $telephony->Telephone_Security . '</td></tr><tr>';

	if ($telephony->TFTP == 'Completed') {$telephony_html .= '<td class="statusOK">';} else {$telephony_html .= '<td class="statusCRITICAL">';}

	$telephony_html .= 'TFTP</td><td>' . $telephony->TFTP . '</td></tr><tr>';

	if ($telephony->Telephone_Registration_Complete == 'Pass') {$telephony_html .= '<td class="statusOK">';}
	elseif ($telephony->Telephone_Registration_Complete == 'Pass With Warnings') {$telephony_html .= '<td class="statusWARNING">';}
	else {$telephony_html .= '<td class="statusCRITICAL">';}

	$telephony_html .= 'REG</td><td>' . $telephony->Telephone_Registration_Complete . '</td></tr></table>';
	$telephony_html .= 'TEL ' . $telephony_status;

	echo $telephony_html;

	doLogout();

	switch ($telephony_status) {
		case 'CRITICAL': exit(STATE_CRITICAL); break;
		case 'WARNING': exit(STATE_WARNING); break;
		case 'OK': exit(STATE_OK); break;
	}
}
?>
